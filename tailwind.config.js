module.exports = {
  content: [
    './*.{html,js}',
  ],
  theme: {
    extend: {
      spacing: {
        '17':'4.25rem'
      }
    },
  },
  plugins: [],
}
